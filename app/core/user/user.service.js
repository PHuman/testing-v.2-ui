'use strict';

angular.
  module('core.user').
  factory('User', ['$resource',
    function($resource) {
      return $resource('http://localhost:8080/usr/:user', {}, {
        query: {
          method: 'POST',
          params: {user: 'user'},
          isArray: false
        }
      });
    }
  ]);