'use strict';

angular.
  module('core').
  filter('typefilter', function() {
    return function(input) {
      return input ? 'checkbox' : 'radio';
    };
  });
