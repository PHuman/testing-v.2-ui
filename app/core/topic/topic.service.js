'use strict';

angular.
module('core.topic').
factory('Topic', ['$resource',
    function($resource) {
        return $resource('http://localhost:8080/usr/topic/:topicId', {}, {
            query: {
                method: 'GET',
                params: { topicId: 'topics' },
                isArray: true,
            }
        });
    }
]);
