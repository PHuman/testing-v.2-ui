'use strict';

angular.
module('core.quiz').
factory('Quiz', ['$resource',
    function($resource) {
        return $resource('http://localhost:8080/usr/topic/:topicId/:quizId/:result', {}, {});
    }
]);
