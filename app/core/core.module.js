'use strict';

// Define the `core` module
angular.module('core', [
	'core.user',
	'core.topic',
	'core.quiz'
	]);