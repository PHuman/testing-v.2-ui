'use strict';

angular.
module('testingApp').
config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.
        when('/', {
            template: '<login-form></login-form>'
        }).
        when('/topic', {
            template: '<topic-list></topic-list>'
        }).
        when('/topic/:topicId', {
            template: '<topic-detail></topic-detail>'
        }).
        when('/topic/:topicId/:quizId', {
            template: '<quiz-detail></quiz-detail>'
        }).
        otherwise('/');
    }
]);
