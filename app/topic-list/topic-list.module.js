'use strict';

// Define the `topicList` module
angular.module('topicList', [
  'ngRoute',
  'core.topic'
]);
