'use strict';

// Register `topicList` component, along with its associated controller and template
angular.
  module('topicList').
  component('topicList', {
    templateUrl: 'topic-list/topic-list.template.html',
    controller: ['Topic',
      function TopicController(Topic) {
       this.topics = Topic.query();
      }
    ]
  });