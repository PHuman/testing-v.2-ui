'use strict';

// Define the `testingApp` module
angular.module('testingApp', [
  'ngRoute',
  'loginForm',
  'topicList',
  'topicDetail',
  'quizDetail',
  'core'
]);
