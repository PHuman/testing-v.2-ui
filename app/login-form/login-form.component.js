'use strict';

// Register `login` component, along with its associated controller and template
angular.
  module('loginForm').
  component('loginForm', {
    templateUrl: 'login-form/login-form.template.html',
    controller: [{},
      function LoginController() {
      }
    ]
  });