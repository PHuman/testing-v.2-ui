'use strict';

// Register `quizDetail` component, along with its associated controller and template
angular.
module('quizDetail').
component('quizDetail', {
    templateUrl: 'quiz-detail/quiz-detail.template.html',
    controller: ['$routeParams', 'Quiz',
        function TopicController($routeParams, Quiz) {
            var self = this;
            self.questions = Quiz.query({ topicId: $routeParams.topicId, quizId: $routeParams.quizId });
            self.submit = function() {
                self.result = Quiz.save({ topicId: $routeParams.topicId, quizId: $routeParams.quizId, result: 'result'}, self.questions);              
            }
        }
    ]
});
