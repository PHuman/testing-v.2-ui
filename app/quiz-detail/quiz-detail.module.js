'use strict';

// Define the `quizDetail` module
angular.module('quizDetail', [
  'ngRoute',
  'core.quiz'
]);