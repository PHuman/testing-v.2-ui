'use strict';

// Define the `topicDetail` module
angular.module('topicDetail', [
  'ngRoute',
  'core.topic'
]);