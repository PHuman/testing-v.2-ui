'use strict';

// Register `topicDetail` component, along with its associated controller and template
angular.
module('topicDetail').
component('topicDetail', {
    templateUrl: 'topic-detail/topic-detail.template.html',
    controller: ['$routeParams', 'Topic',
        function TopicController($routeParams, Topic) {
            var self = this;
            self.quiz = Topic.query({ topicId: $routeParams.topicId});
            self.topic = $routeParams.topicId;
        }
    ]
});
